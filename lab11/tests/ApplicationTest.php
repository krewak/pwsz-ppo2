<?php

use App\Controllers\Controller;
use App\Helpers\Result;
use PHPUnit\Framework\TestCase;

final class ApplicationTest extends TestCase {

	public function testIfTrueIsTrue(): void {
		$this->assertTrue(true);
	}

	public function testIfEmptyRequestIsHandledProperly(): void {
		$result = $this->getResultFromController([]);
		$this->assertNull($result);
	}

	public function testIfInvalidRequestIsHandledProperly(): void {
		$result = $this->getResultFromController(["test" => "test"]);
		$this->assertNull($result);
	}

	public function testIfQuestionActionIsHandledProperly(): void {
		$result = $this->getResultFromController(["action" => "quote"]);
		$this->assertNotNull($result);
		$this->assertInstanceOf(Result::class, $result);
		$this->assertContains($result->content, Controller::QUOTES);
	}

	protected function getResultFromController(array $request): ?Result {
		$controller = new Controller();
		return $controller->getResult($request);
	}

}
